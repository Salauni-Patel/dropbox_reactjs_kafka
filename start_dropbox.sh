#!/usr/bin/env bash
dir=$(pwd)


cd $dir/source/mongodb-osx-x86_64-3.4.10/bin && mongod --fork  --logpath $dir/source/mongodb-osx-x86_64-3.4.10/log/mongodb.log --dbpath $dir/source/mongodb-osx-x86_64-3.4.10/data --port 27018
sleep 2

cd  $dir/source/kafka_2.10-0.9.0.1 > /dev/null 2>&1 & 
cd  $dir/bin/zookeeper-server-start.sh config/zookeeper.properties  > /dev/null 2>&1 &
sleep 2

cd  $dir/source/kafka_2.10-0.9.0.1 > /dev/null 2>&1 &
cd  $dir/bin/kafka-server-start.sh config/server.properties  > /dev/null 2>&1 &
sleep 2

cd  $dir/source/kafka_2.10-0.9.0.1 && bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 0 --topic activity-topic
cd  $dir/source/kafka_2.10-0.9.0.1 && bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic fileslist-topic
sleep 2

npm start