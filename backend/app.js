var Kafka = require('no-kafka');
var producer = new Kafka.Producer();
var consumer = new Kafka.SimpleConsumer();

var c = require('./services/kafka-consumer')
var fs = require('fs'),
    express = require('express'),
    expressSessions = require("express-session"),
    fileUpload = require('express-fileupload'),
    path = require('path'),
    favicon = require('serve-favicon'),
    morgan = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    uuidv4 = require('uuid/v4'),
    passport = require('passport'),
    index = require('./routes/index'),
    db_service = require('./services/service-db'),
    mongoStore = require("connect-mongo")(expressSessions),
    logger = require('logger').createLogger();
// require('events').EventEmitter.defaultMaxListeners = 0;
require('./routes/passport')(passport);

var app = express();



app.use(fileUpload());


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../frontend/public')));

app.use(expressSessions({
    secret: "Salauni_KEY",
    resave: false,
    saveUninitialized: true,
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 6 * 1000,
    store: new mongoStore({
        url: "mongodb://localhost:27017/dropbox"
    })
}));


app.use(passport.initialize());
app.use(passport.session());

/* ROUTES */
app.use('/', index);


// api

app.post('/dropbox_upload', function (req, res) {
    var sampleFile = req.files.sampleFile;
    var filename = uuidv4() + '_' + sampleFile.name;
    var messages = {
        name: "upload",
        value: filename,
        correlationId: uuidv4()

    };


     producer.init().then(function(){
         producer.send({
            topic: 'fileslist-topic',
            partition: 0,
            message: {
                value: JSON.stringify(messages)
            }
        });
    })
        .then(function (result) {

        });


    if (!req.files)
        return res.status(400).send('FAILED');


    // mv is for moving file to a particular place in server
    sampleFile.mv(path.join(__dirname, '../frontend/public/upload') + '/' + filename, function (err) {
        if (err) return res.status(500).send('FAILED');
        db_service.findUser(req.body.loggedInUserEmail, function (user) {
            logger.info(user)
            if (user.length > 0) {
                db_service.saveFile(filename, user[0]._id)
                res.send('SUCCESS');
            }
        });
    });
});

app.post("/dropbox_files", function (req, res) {
    db_service.findUser(req.body.loggedInUserEmail, function (user) {
        if (user.length > 0) {
            var ownerId = user[0]._id;
            db_service.findFiles(ownerId, function (data) {
                res.send(data);
            })
        }
    });
});

app.post("/dropbox_activity", function (req, res) {
    db_service.activity(function (data) {
        res.send(data);
    });
});




app.post("/dropbox_directory", function (req, res) {
    db_service.findUser(req.body.loggedInUserEmail, function (user) {
        if (user.length > 0) {
            var ownerId = user[0]._id;
            db_service.findFiles(ownerId, function (data) {
                res.send(data);
            })
        }
    });
});





// kafka PRODUCER for DIRECTORY

app.get('/dropbox_directory', function (req, res) {
    // return promise
    var dataHandler = function (messageSet, topic, partition) {
        var arr = [];
        messageSet.forEach(function (m) {
            console.log(topic, partition, m.offset, m.message.value.toString('utf8'));
            arr.push( JSON.parse( m.message.value.toString('utf8')))
        });

        res.send(arr);
    };

     consumer.init().then(function () {
       
         consumer.subscribe('fileslist-topic',[0] , {time: Kafka.EARLIEST_OFFSET}, dataHandler);
    });
});
// kafka PRODUCER for Route events

app.post("/dropbox_route_events", function (req, res) {
    var messages = {
        name: req.body.name,
        value: req.body.value,
        correlationId: uuidv4()

    };


    return producer.init().then(function(){
        return producer.send({
            topic: 'activity-topic',
            partition: 0,
            message: {
                value: JSON.stringify(messages)
            }
        });
    })
        .then(function (result) {
            res.send(result);
        });


});

app.post("/shared_files_by_others", function (req, res) {
    db_service.sharedFilesByOthers(req.body.loggedInUserEmail, function (data) {
        res.send(data);
    });
});

app.post("/dropbox_share_file", function (req, res) {
    db_service.findUser(req.body.loggedInUserEmail, function (user) {
        console.log(user)
        var ownerId = user[0]._id;
        var fileId = req.body.fileId;
        var emailId = req.body.emailId;
        logger.info(req.body, ownerId, fileId, emailId)
        db_service.shareFile(ownerId, fileId, emailId, function (data) {
            res.send(data);
        })
    });

});

app.post('/dropbox_login', function (req, res) {
    passport.authenticate('login', function (err, user) {
        logger.info(err, user)
        if (err) {
            return res.status(500).send("error");
        } else if (!user) {
            return res.status(401).send("Invalid user credentials");
        } else {
            return res.status(200).send({username: user.username});
        }
    })(req, res);
});

app.post('/dropbox_register', function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    var phone = req.body.phone;
    var name = req.body.name;
    db_service.findUser(email, function (user) {
        logger.info(user)
        if (user.length == 0) {
            db_service.saveUser(email, password, name, phone, function (data) {
                return res.send(data);
            })
        } else {
            return res.status(401).send("user exists");
        }
    });


});

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    res.redirect("/")
});

app.use(function (err, req, res, next) {
    
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
