var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/dropbox');
var Schema = mongoose.Schema, ObjectId = Schema.ObjectId;
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var UserSchemaModel = require('./../services/service-db').UserSchemaModel;

module.exports = function (passport) {
    passport.use('login', new LocalStrategy(function (username, password, done) {
        UserSchemaModel.find({email: username, password: password}, function (err, user) {
            console.log("inside passport")
            console.log(err, user)
            if (err) {
                done(null, false);
            } else if (user.length > 0) {
                done(null, {username: username, password: password});
            } else {
                done(null, false);
            }
        });
    }));
};



