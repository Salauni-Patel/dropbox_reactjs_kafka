var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/dropbox');
var Schema = mongoose.Schema, ObjectId = Schema.ObjectId;
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var uuidv4 = require('uuid/v4');

var UserSchema = new Schema({
    id: ObjectId,
    email: String,
    password: String,
    phone : String,
    name : String,
    role: [{
        type: String
    }]
});

var FileSchema = new Schema({
    id: ObjectId,
    filename: String,
    ownerId: ObjectId,
    sharedWith: [String]
});

var EventSchema = new Schema({
    id: ObjectId,
    name: String,
    correlationId: String,
    value: Object
});

var UserSchemaModel = mongoose.model('User', UserSchema);
var FileSchemaModel = mongoose.model('File', FileSchema);
var EventSchemaModel = mongoose.model('Event', EventSchema);

var services = {
    UserSchemaModel : UserSchemaModel,

    directory : function (ownerId, cb) {
        FileSchemaModel.find({'ownerId':ownerId}, function (err, data) {
            if (err) console.log(err);
            if (cb) cb(data);
        });
    },
    

    activity : function (cb) {
        EventSchemaModel.find({}, function (err, event) {
            if (err) console.log(err);
            if (cb) cb(event);
        });
    },
    saveEvent: function (py, cb) {
        EventSchemaModel.find({'correlationId': py.correlationId}, function (err, event) {
            if (err) {console.log(err)}
            else{
                if(!event || event.length == 0) {
                    var model = new EventSchemaModel();
                    model.name = py.name;
                    model.value = py.value;
                    model.correlationId = uuidv4();

                    model.save(function (err, data) {
                        if (err) console.log(err);
                        if (cb) cb(event);
                    });
                }
            }
        });


    },
    saveFile: function (filename, ownerId) {
        var model = new FileSchemaModel();
        model.ownerId = ownerId;
        model.filename = filename;
        model.save(function (err) {
            console.log(err)
            if (!err) console.log('Success!');
        });
    },
    saveUser: function (email, password, name, phone, cb) {
        var model = new UserSchemaModel();
        model.email = email;
        model.password = password;
        model.phone = phone;
        model.name = name;
        model.role = ["ROLE_USER"];

        model.save(function (err, data) {
            if (err) console.log(err);
            if (cb) cb(data);
        });
    },
    findUser: function (email, cb) {
        UserSchemaModel.find({'email': email}, function (err, data) {
            if (err) console.log(err);
            if (cb) cb(data);
        });
    },
    findFiles: function (ownerId, cb) {
        FileSchemaModel.find({'ownerId': ownerId}, function (err, data) {
            if (err) console.log(err);
            if (cb) cb(data);
        });
    },
    shareFile: function (ownerId, fileId, emailId, cb) {
        FileSchemaModel.update(
            {"ownerId": ownerId, '_id': mongoose.mongo.ObjectId(fileId)},
            {"$push": {"sharedWith": emailId}},
            function (err, data) {
                if (err) console.log(err);
                if (cb) cb(data);
            }
        );
    },
    sharedFilesByOthers: function (emailId, cb) {
        FileSchemaModel.find(
            {"sharedWith": emailId},
            function (err, data) {
                if (err) console.log(err);
                if (cb) cb(data);
            }
        );
    },
    login: function (passport) {
        passport.use('login', new LocalStrategy(function (username, password, done) {
            try {
                UserSchemaModel.find({username: username, password: password}, function (err, user) {
                    if (err){
                        console.log(err);
                        done(null, false);
                    }else if (user) {
                        done(null, {username: username, password: password});
                    } else {
                        done(null, false);
                    }
                });
            }
            catch (e) {
                done(e, {});
            }
        }));
    }
}

var conn_idx = 1;

var pool = pooling.createPool({
   checkInterval: 1 * 1000,
   max: 2,
   maxIdleTime: 30*1000,
   name : 'my pool',
   create : function create(callback) {
       var client = new EventEmitter();
       client.id = conn_idx++;
       Helpers.logInfo('Creating pool client id: ' + client.id);
        
       return callback(null, client);
   },
   destroy : function destroy(client) {
       Helpers.logInfo('Destroyed pool client id: ' + client.id);
       client.was = client.id;
       client.id = -1;
   }
});
module.exports = services;
