var Kafka = require('no-kafka');
var db_service = require('./service-db');

var consumer = new Kafka.SimpleConsumer();

// data handler returns Promise
var dataHandler = function (messageSet, topic, partition) {
    messageSet.forEach(function (m) {
        var py =  JSON.parse(m.message.value.toString('utf8'));
        console.log(py)
        db_service.saveEvent(py, function (event) {
            console.log("getting data", event);
        }); 
    });
};

return consumer.init().then(function () {
    
    return consumer.subscribe('activity-topic', [0], dataHandler);
});