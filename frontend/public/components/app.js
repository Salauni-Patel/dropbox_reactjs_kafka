import React from "react";
import {Link, Route, Router} from "react-router-dom";
//import {ResponsiveEmbed, Image} from 'react-bootstrap';
import createHistory from "history/createBrowserHistory";
//import Center from 'react-center';


const history = createHistory()
history.listen((location, action) => {

    // call kafka activity api
    if(location.pathname != '/dropbox_activity') {
        var payload = {
            name: "route",
            value: location
        }

        fetch(`dropbox_route_events`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            }
        )
            .then(res => {

                console.log(res)
            })
            .catch(error => {

            });
    }


    
    var loggedInUserEmail = sessionStorage.getItem("loggedInUserEmail");
    if (loggedInUserEmail && loggedInUserEmail.length > 0) {
        $('[href="/dropbox_login"]').hide()
        $('[href="/dropbox_signup"]').hide()
        $('[href="/logout"]').show()
        $('[id="sidebar-wrapper"]').show()
        document.getElementsByClassName("main-content")[0].style.marginLeft = "200px";

    } else {
        $('[href="/dropbox_login"]').show()
        $('[href="/dropbox_signup"]').show()
        $('[href="/logout"]').hide()
        $('[id="sidebar-wrapper"]').hide()
        document.getElementsByClassName("main-content")[0].style.marginLeft = "0px";

    }

    
    if (location.pathname == '/logout') {
        sessionStorage.clear();
        history.push('/dropbox_login')
    } else {
    
        var protectedRoutes = ['/dropbox_files', '/dropbox_share', '/home','/dropbox_directory'];
        if (protectedRoutes.indexOf(location.pathname) != -1) {
            if (!loggedInUserEmail || loggedInUserEmail.length == 0) {
                history.push('/dropbox_login')
            }
        }

    }


});

class App extends React.Component {

    componentDidMount() {
        console.log(this)
    }

    render() {
        return (
            <Router history={history}>
                <div>
                    <nav className="navbar navbar-default">
                        <div className="container" >
                            <div className="navbar-header">
                                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span className="sr-only">Toggle navigation</span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                                <a className="navbar-brand" href="/">
                                
                                   <i> <img className="w3-card-2 w3-round" style={{height: 25, width:25} } src={'./components/icon.png'}/> My Drop Box</i>
                                </a>
                            </div>

                            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul className="nav navbar-nav navbar-right">
                                    <li><Link to="/dropbox_login">Login</Link></li>
                                    <li><Link to="/dropbox_signup">Register</Link></li>
                                    <li><i><input type="text" placeholder="search" /></i></li>
                                    <li><Link to="/logout">Logout</Link></li>

                                </ul>
                            </div>
                        </div>
                    </nav>
                    <div id="sidebar-wrapper" className="sidebar-toggle" >
                        <ul className="sidebar-nav" >
                            <li><Link to="/dropbox_directory">My Uploaded Files</Link></li>
                            <li><Link to="/dropbox_files">My Shared Files</Link></li>
                            <li><Link to="/starred">My Starred Files</Link></li>
                            <li><Link to="/dropbox_activity">Activity Log</Link></li>
                        </ul>
                    </div>
                    <div id="sidebar-wrapper" className="rt-13" >
                        <ul className="sidebar-nav" >
                            <li><Link to="/profile">My Profile</Link></li>
                            <li><Link to="/home">Upload Files</Link></li>
                            <li><Link to="/dropbox_files">Share Files</Link></li>
                            <li><Link to="/group">New Group</Link></li>
                            </ul>
                    </div>

                    <div className="main-content">
                        <Route exact path="/" component={Welcome}/>
                        <Route path="/profile" component={profile}/>
                        <Route path="/home" component={Home}/>
                        <Route path="/dropbox_files" component={Files}/>
                        <Route path="/dropbox_share" component={Share}/>
                        <Route path="/dropbox_login" component={Login}/>
                        <Route path="/dropbox_signup" component={Signup}/>
                        <Route path="/dropbox_activity" component={Activity}/>
                        <Route path="/dropbox_directory" component={Directory}/>
                        <Route path="/starred" component={Starred}/>
                        <Route path="/group" component={Group}/>
                       
                    </div>

                </div>
            </Router>
        )
    }
}

class Activity extends React.Component {

    constructor() {
        super();
        this.state = {
            activities: []
        }

    }
    componentDidMount() {
        var self = this;
        fetch(`dropbox_activity`, {
                method: 'POST'
        }
        )
            .then((resp) => resp.json()) 
            .then(res => {
                self.setState({activities: res});

                console.log(res)
            })
            .catch(error => {

            });
    }

    render() {
        return (
            <div className="container">
                <table className="table table-striped">
                    <thead>
                    <tr style={{'backgroundColor':'#bcd2f2'}}>
                        <th>#</th>
                        <th>Type</th>
                        <th>Destination page</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.activities.map((item, index) => (
                        <tr key={item._id}>
                            <th scope="row">{index + 1}</th>
                            <td>{item.name}</td>
                            <td>{item.value.pathname}</td>
                        </tr>
                    ))}

                    </tbody>
                </table>
            </div>
        )
    }
}


class Directory1 extends React.Component {
    
        
    
        render() {
            return (
                <div className="container">
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Activity</th>
                            <th>File Name</th>
                        </tr>
                        </thead>
                       
                    </table>
                </div>
            )
        }
    }




    class Directory extends React.Component {
        
        constructor() {
            super();
            this.state = {
                files: [],
                //sharedfiles: [],
                selectedShareFile: null,
                emailId: null
            }
    
        }
        share(event) {
            var payload = {
                emailId: this.state.emailId,
                fileId: this.state.selectedShareFile,
                loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
            }
    
            console.log(payload)
            var self = this;
            fetch(`dropbox_share_file`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(payload)
                }
            )
                .then(res => {
    
                    console.log(res)
                })
                .catch(error => {
    
                });
    
        }
    
        componentDidMount() {
            var self = this;
            fetch(`dropbox_directory`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
                    })
    
                }
            )
                .then((resp) => resp.json()) 
                .then(res => {
                    self.setState({files: res});
    
                    console.log(res)
                })
                .catch(error => {
    
                    
                });
            }
        
            render() {
                return (
                    <div>
                    <h2><small>My files and folders</small></h2>
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>File name</th>
                            <th>Star File</th>
                            <th>Download</th>
                            <th>Share</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.files.map((item, index) => (
                            <tr key={item._id}>
                                <th scope="row">{index + 1}</th>
                                <td>{item.filename.split('_')[1]}</td>
                                <td>
                                <button className="btn btn-info btn-sm">
                                <span className="glyphicon glyphicon-star" aria-hidden="true"></span> Star
                                    </button>  
                                  </td>
                                <td>
                                    <a href={'/upload/' + item.filename} target="_blank"><button className="btn btn-info btn-sm">Download</button> </a>
                                </td>
                                <td>
                                    <button className="btn btn-info btn-sm" data-toggle="modal"
                                            data-target=".bs-example-modal-sm"
                                            onClick={(event) => {
                                                this.setState({
                                                    selectedShareFile: item._id
                                                });
                                            }}
                                    >Share
                                    </button>
                                </td>
                            </tr>
                        ))}
    
                        </tbody>
                    </table>
                    <div className="modal fade bs-example-modal-sm" role="dialog" aria-labelledby="mySmallModalLabel">
                        <div className="modal-dialog modal-sm" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                    <h4 className="modal-title" id="myModalLabel">Share file with</h4>
                                </div>
                                <div className="modal-body">
                                    <div className="input-group">
                                        <span className="input-group-addon" id="basic-addon3">Enter username</span>
                                        <input type="text" className="form-control" id="basic-url"
                                               aria-describedby="basic-addon3"
                                               onChange={(event) => {
                                                   this.setState({
                                                       emailId: event.target.value
                                                   });
                                               }}
                                        ></input>
                                    </div>
                    
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" className="btn btn-info" onClick={this.share.bind(this)}>Share
                                        Now
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                )
            }
        }






        //code for star files

        class Starred extends React.Component {
            
            constructor() {
                super();
                this.state = {
                    files: [],
                    //sharedfiles: [],
                    selectedShareFile: null,
                    emailId: null
                }
        
            }
            share(event) {
                var payload = {
                    emailId: this.state.emailId,
                    fileId: this.state.selectedShareFile,
                    loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
                }
        
                console.log(payload)
                var self = this;
                fetch(`dropbox_share_file`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(payload)
                    }
                )
                    .then(res => {
        
                        console.log(res)
                    })
                    .catch(error => {
        
                    });
        
            }
        
            componentDidMount() {
                var self = this;
                fetch(`dropbox_directory`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
                        })
        
                    }
                )
                    .then((resp) => resp.json()) 
                    .then(res => {
                        self.setState({files: res});
        
                        console.log(res)
                    })
                    .catch(error => {
        
                        
                    });
                }
            
                render() {
                    return (
                        <div>
                        <h2><small>My Starred files</small></h2>
                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>File name</th>
                                <th>Remove Star</th>
                                <th>Download</th>
                                <th>Share</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.files.map((item, index) => (
                                <tr key={item._id}>
                                    <th scope="row">{index + 1}</th>
                                    <td>{item.filename.split('_')[1]}</td>
                                    <td>
                                        <button className="btn btn-info btn-sm">Remove</button>
                                    </td>
                                    <td>
                                        <a href={'/upload/' + item.filename} target="_blank"><button className="btn btn-info btn-sm">Download</button> </a>
                                    </td>
                                    <td>
                                        <button className="btn btn-info btn-sm" data-toggle="modal"
                                                data-target=".bs-example-modal-sm"
                                                onClick={(event) => {
                                                    this.setState({
                                                        selectedShareFile: item._id
                                                    });
                                                }}
                                        >Share
                                        </button>
                                    </td>
                                </tr>
                            ))}
        
                            </tbody>
                        </table>
                        <div className="modal fade bs-example-modal-sm" role="dialog" aria-labelledby="mySmallModalLabel">
                            <div className="modal-dialog modal-sm" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                        <h4 className="modal-title" id="myModalLabel">Share file with</h4>
                                    </div>
                                    <div className="modal-body">
                                        <div className="input-group">
                                            <span className="input-group-addon" id="basic-addon3">Enter email id</span>
                                            <input type="text" className="form-control" id="basic-url"
                                                   aria-describedby="basic-addon3"
                                                   onChange={(event) => {
                                                       this.setState({
                                                           emailId: event.target.value
                                                       });
                                                   }}
                                            ></input>
                                        </div>
                        
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" className="btn btn-info" onClick={this.share.bind(this)}>Share
                                            Now
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    )
                }
            }
    





class Home extends React.Component {

    componentDidMount() {
        console.log(this)
    }

    handleSubmit(event) {

        event.preventDefault();
        var self = this;
        if (this.refs.sampleFile !== '') {
            const payload = new FormData();

            payload.append('sampleFile', this.refs.sampleFile.files[0]);
            payload.append('loggedInUserEmail', sessionStorage.getItem("loggedInUserEmail"))

            fetch(`dropbox_upload`, {
                    method: 'POST',
                    headers: {
                        // 'Content-Type': 'multipart/form-data'
                    },
                    body: payload
                }
            )
                .then(res => {
                    if (res.status == 200) {
                        alert("file uploaded")
                    } else if (res.status == 400 || res.status == 500) {
                        alert("file upload failed")
                    }

                    $("#sampleFile").val('');


                })
                .catch(error => {

                });

            return;
        }
    }

    render() {
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="row">
                        <div className="col-md-6">
                            <input className="form-control" ref="sampleFile" type="file" name="sampleFile" id="sampleFile"/>

                        </div>
                        <div className="col-md-6">
                            <input className="btn btn-info" type="submit" value="Upload!"/>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class Files extends React.Component {
    constructor() {
        super();
        this.state = {
            files: [],
            sharedfiles: [],
            selectedShareFile: null,
            emailId: null
        }
        this.handleClick = this.handleClick.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        alert("Created group");
    }
handleClick() {
    alert("hi");
  }

    share(event) {
        var payload = {
            emailId: this.state.emailId,
            fileId: this.state.selectedShareFile,
            loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
        }

        console.log(payload)
        var self = this;
        fetch(`dropbox_share_file`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            }
        )
            .then(res => {

                console.log(res)
            })
            .catch(error => {

            });

    }

    componentDidMount() {
        var self = this;
        fetch(`dropbox_files`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
                })

            }
        )
            .then((resp) => resp.json()) 
            .then(res => {
                self.setState({files: res});

                console.log(res)
            })
            .catch(error => {

            });

        fetch(`shared_files_by_others`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
                })
            }
        )
            .then((resp) => resp.json()) 
            .then(res => {
                self.setState({sharedfiles: res});

                console.log(res)
            })
            .catch(error => {

            });


    }


    render() {
        return (
            <div>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>File name</th>
                        <th>Star File</th>
                        <th>Download</th>
                        <th>Share</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.files.map((item, index) => (
                        <tr key={item._id}>
                            <th scope="row">{index + 1}</th>
                            <td>{item.filename.split('_')[1]}</td>
                            <td>
                              <button className="btn btn-info btn-sm">
                              <span className="glyphicon glyphicon-star" aria-hidden="true"></span> Star
                                  </button>  
                            </td>
                            <td>
                                <a href={'/upload/' + item.filename} target="_blank"><button className="btn btn-info btn-sm">Download</button> </a>
                            </td>
                            <td>
                                <button className="btn btn-info btn-sm" data-toggle="modal"
                                        data-target=".bs-example-modal-sm"
                                        onClick={(event) => {
                                            this.setState({
                                                selectedShareFile: item._id
                                            });
                                        }}
                                >Share
                                </button>
                            </td>
                        </tr>
                    ))}

                    </tbody>
                </table>
                <hr/>
                Share files
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>File name</th>
                        <th>Star File</th>
                        <th>Download</th>
                        <th>Share</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.sharedfiles.map((item, index) => (
                        <tr key={item._id}>
                            <th scope="row">{index + 1}</th>
                            <td>{item.filename.split('_')[1]}</td>
                            <td>
                            <button className="btn btn-info btn-sm" onClick={this.handleClick}>
                            <span className="glyphicon glyphicon-star" aria-hidden="true"></span> Star
                                </button>  
                                                     </td>
                            <td>
                                <a href={'/upload/' + item.filename} target="_blank"><button className="btn btn-info btn-sm">Download</button> </a>
                            </td>
                            <td>
                                <button className="btn btn-info btn-sm" data-toggle="modal"
                                        data-target=".bs-example-modal-sm">Share Now
                                </button>
                            </td>
                        </tr>
                    ))}

                    </tbody>
                </table>

                <div className="modal fade bs-example-modal-sm" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div className="modal-dialog modal-sm" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel">Share file with</h4>
                            </div>
                            <div className="modal-body">
                                <div className="input-group">
                                    <span className="input-group-addon" id="basic-addon3">Enter email id</span>
                                    <input type="text" className="form-control" id="basic-url"
                                           aria-describedby="basic-addon3"
                                           onChange={(event) => {
                                               this.setState({
                                                   emailId: event.target.value
                                               });
                                           }}
                                    ></input>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-info" onClick={this.share.bind(this)}>Share
                                    Now
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Signup extends React.Component {

    constructor() {
        super();

        this.state = {
            password: "",
            email: "",
            phone: "",
            name: ""
        }
    }

    componentDidMount() {
        console.log(this)
    }

    handleSubmit(event) {
        event.preventDefault();

        fetch(`dropbox_register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: this.state.email,
                    password: this.state.password,
                    phone: this.state.phone,
                    name: this.state.name
                })
            }
        )
            .then(res => {
                if (res.status == 200) {
                    alert("Sucessfully registered")
                    history.push('/dropbox_login');
                } else if (res.status == 400 || res.status == 500) {
                }

            })
            .catch(error => {

            });

    }


    render() {
        return (
            <div className="container">

<div className="col-md-5">
                <img src={'./components/dropboxLogin.png'}/>
                </div>

                <div className="col-md-1">

                </div>
                <div className="col-md-6">
                    <form onSubmit={this.handleSubmit.bind(this)}>
                        <h3 className="form-signin-heading"> Register for DropBox Now.</h3>
                        
                        <br/>

                        <input value={this.state.email}
                               onChange={(event) => {
                                   this.setState({
                                       email: event.target.value
                                   });
                               }} type="text" className="form-control" name="Username" placeholder="Username"
                               required=""
                               autoFocus=""/>
                        <br/>
                        <input value={this.state.password}
                               onChange={(event) => {
                                   this.setState({
                                       password: event.target.value
                                   });
                               }} type="password" className="form-control" name="Password" placeholder="Password"
                               required=""/>
                        <br/>

                        <input value={this.state.name}
                               onChange={(event) => {
                                   this.setState({
                                       name: event.target.value
                                   });
                               }} type="text" className="form-control" placeholder="Your Name"
                               required=""/>
                        <br/>

                        <input value={this.state.phone}
                               onChange={(event) => {
                                   this.setState({
                                       phone: event.target.value
                                   });
                               }} type="text" className="form-control" placeholder="Enter phone"
                               required=""/>
                        <br/>

                        <button className="btn btn-lg btn-info btn-block" type="submit" value="Register">Register
                        </button>
                    </form>
                </div>

            </div>
        )
    }
}

class Login extends React.Component {

    constructor() {
        super();

        this.state = {
            password: "",
            username: ""
        }
    }

    componentDidMount() {
        sessionStorage.clear();
    }

    handleSubmit(event) {
        event.preventDefault();

        fetch(`dropbox_login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: this.state.username,
                    password: this.state.password
                })
            }
        )
            .then((resp) => resp.json()) 
            .then(res => {
                sessionStorage.setItem("loggedInUserEmail", res.username)
                history.push('/home')
            })
            .catch(error => {

            });

    }

    render() {
        return (
            <div className="container">
                <div className="col-md-5">
                <img src={'./components/dropboxLogin.png'}/>
                </div>

                <div className="col-md-1"></div>

                <div className="col-md-6">

                <form onSubmit={this.handleSubmit.bind(this)}>
                        <h3 className="form-signin-heading">Login to your dropbox</h3>
                        <hr/>
                        <br/>

                        <input value={this.state.username}
                               onChange={(event) => {
                                   this.setState({
                                       username: event.target.value
                                   });
                               }} type="text" className="form-control" name="Username" placeholder="Username"
                               required=""
                               autoFocus=""/>
                        <br/>
                        <input value={this.state.password}
                               onChange={(event) => {
                                   this.setState({
                                       password: event.target.value
                                   });
                               }} type="password" className="form-control" name="Password" placeholder="Password"
                               required=""/>

                        <br/>
                        <button className="btn btn-lg btn-info btn-block" type="submit" value="Login">Login</button>
                    </form>
                </div>


               

                    
                </div>

            
        )
    }
}

class Welcome extends React.Component {


    render() {
        return (
           
            <div className="container">

                <div className="col-md-5">
                <img src={'./components/dropboxLogin.png'}/>
                </div>
                <div className="col-md-7">
               <h1> Welcome to dropbox</h1>
               <h2>Please login to your account to access your files.</h2>
               <h2> Not a user? Signup today...</h2>
                </div>
            </div>
            
        )
    }
}



class profile extends React.Component {
    
    
        render() {
            return (
                <div className="container">
                    
                  <i>  <h1>Your Profile</h1>
                  <button className="btn btn-info btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm">
                            <span className="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                                </button>
                  </i>

               
<div className="modal fade bs-example-modal-sm" id="myModal" role="dialog" aria-labelledby="exampleModalLabel">
  <div className="modal-dialog modal-sm" role="document">
    <div className="modal-content">
      <div className="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button><h1>Edit Your Information</h1>
      </div>
      <div className="modal-body">
    

        <div className="input-group">
                                   
                                    <input type="text" className="form-control" placeholder="New Username" id="basic-url"
                                           aria-describedby="basic-addon3"></input><br/>
                                     <input type="text" className="form-control" placeholder="New Password" id="basic-url"
                                           aria-describedby="basic-addon3"></input><br/>
                                          
                                    <input type="text" className="form-control" placeholder="Contact Number" id="basic-url"
                                           aria-describedby="basic-addon3"></input><br/>
                                           
        </div>

      </div>
      <div className="modal-footer">
       
        <button type="button" className="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
                   
                   <table className="table table-striped">
                       <tbody>
                       <tr>
                           <td>Name:</td>
                           <td>Salauni</td>
                        </tr>
                        <tr>
                           <td>UserName:</td>
                           <td>Salauni</td>
                        </tr>
                        <tr>
                           <td>Birth Date:</td>
                           <td>02/02/1995</td>
                        </tr>
                        <tr>
                           <td>Contact Number:</td>
                           <td>6692121906</td>
                        </tr>
                        </tbody>
</table>                   
    
                </div>
            )
        }
    }
    

class Group extends React.Component {
    
    handleSubmit(event) {
        event.preventDefault();
        alert("Created group");
    }

        render() {
            return (
                <div className="container">
                    <h2>Create your group now.</h2>
                    <form onSubmit={this.handleSubmit.bind(this)}>
                    
                    <input type="text" className="form-control" placeholder="enter group name" /><br/>
                    <input type="text" className="form-control" placeholder="enter username" /><br/>
                    <button className="btn btn-info btm-sm">Create Group</button>
                    
                     </form>
    
                </div>
            )
        }
    }


const Share = ({match}) => (
    <div>
        <h2>Share</h2>
        <ul>
            <li>
                <Link to={`${match.url}/rendering`}>
                    Rendering with React
                </Link>
            </li>
            <li>
                <Link to={`${match.url}/components`}>
                    Components
                </Link>
            </li>
            <li>
                <Link to={`${match.url}/props-v-state`}>
                    Props v. State
                </Link>
            </li>
        </ul>

        <Route path={`${match.url}/:topicId`} component={Topic}/>
        <Route exact path={match.url} render={() => (
            <h3>Please select a topic.</h3>
        )}/>
    </div>
);

const Topic = ({match}) => (
    <div>
        <h3>{match.params.topicId}</h3>
    </div>
);

export default App